(function ($) {

  $('#btnLoadText').click(function () { $("#showResult").load("show.txt"); });
  $('#btnAjax').click(function () { callRestAPI() });

  // Perform an asynchronous HTTP (Ajax) API request.
  function callRestAPI() {
    var root = 'http://ip.jsontest.com/';
    $.ajax({
      url: root,
      method: 'GET'
    }).then(function (response) {
      console.log(response);
      $('#showResult').html('<font color="red"><h3>The IP Address of your machine: '+response.ip+'</h3></font>');
      
    });
  }
})($);